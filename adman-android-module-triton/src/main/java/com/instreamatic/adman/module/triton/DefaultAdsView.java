package com.instreamatic.adman.module.triton;

import android.app.Activity;

import com.instreamatic.adman.view.IAdmanViewBundleFactory;


/**
 * This sample shows a custom UI implementation.
 *
 * - Ads must be loaded at each play request so the server can update the tracking URLs.
 * - No support will be given on custom ad playback.
 * - Make sure to call the tracking methods from AdUtil at the right place or the ads might not be paid.
 */
public class DefaultAdsView extends TritonBaseAdsView {

    final private static String TAG = "DefaultAdsView";

    private IAdmanViewBundleFactory factory;

    public DefaultAdsView(Activity activity) {
        super(activity);
        factory = new DefaultAdsViewBindFactory();
    }

    @Override
    public IAdmanViewBundleFactory factory() {
        return factory;
    }

}
