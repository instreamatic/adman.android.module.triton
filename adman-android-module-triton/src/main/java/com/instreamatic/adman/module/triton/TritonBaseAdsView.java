package com.instreamatic.adman.module.triton;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.instreamatic.adman.ActionUtil;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.event.PlayerEvent;
import com.instreamatic.adman.module.BaseAdmanModule;
import com.instreamatic.adman.view.AdmanLayoutType;
import com.instreamatic.adman.view.AdmanViewType;
import com.instreamatic.adman.view.IAdmanView;
import com.instreamatic.adman.view.IAdmanViewBundle;
import com.instreamatic.adman.view.IAdmanViewBundleFactory;
import com.instreamatic.player.IAudioPlayer;
import com.instreamatic.vast.model.VASTInline;
import com.tritondigital.ads.Ad;
import com.tritondigital.ads.SyncBannerView;

import java.lang.ref.WeakReference;


/**
 * Shows how to display an on-demand ad.
 */
abstract class TritonBaseAdsView extends BaseAdmanModule
        implements IAdmanView, PlayerEvent.Listener, AdmanEvent.Listener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    final public static String ID = TritonBaseAdsView.class.getSimpleName();
    final private static String TAG = ID;

    protected WeakReference<Activity> contextRef;
    protected View view;
    protected ViewGroup target;
    private IAdmanViewBundle bundle;

    protected boolean displaying;

    private SyncBannerView mBannerView;
    private SurfaceView mVideoView;
    private SurfaceHolderCallback holderSurfaceCallback = new SurfaceHolderCallback();

    private enum StateSuitableVAST {
        NONE,
        TRUE,
        FALSE
    }
    private StateSuitableVAST stateSuitableVAST = StateSuitableVAST.NONE;

    public TritonBaseAdsView(Activity context) {
        displaying = false;
        setActivity(context);
    }

    public void setActivity(Activity activity){
        WeakReference<Activity> weakRef = this.contextRef;
        this.contextRef = new WeakReference<>(activity);
        if (weakRef != null) {
            weakRef.clear();
        }
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[] {PlayerEvent.TYPE, AdmanEvent.TYPE};
    }

    @Override
    public int eventPriority() {
        return super.eventPriority() + 200;
    }

    @Override
    public String getId() {
        return ID;
    }

    public View getView() {
        return this.bundle != null ? this.bundle.get(AdmanViewType.CONTAINER) : null;
    }

    public void setTarget(ViewGroup var1) {
        this.target = target;
    }

    public ViewGroup getTarget() {
        if (this.target != null) {
            return this.target;
        } else {
            Activity context = this.contextRef.get();
            if(context == null) {
                Log.w(TAG, "Activity is null");
                return null;
            }
            return (ViewGroup) context.getWindow().getDecorView().findViewById(android.R.id.content);
        }
    }

    public void show() {
        Activity context = contextRef.get();
        if (context == null || !isSuitableVAST()) {
            Log.w(TAG, "Ad loading ERROR: NULL ad");
            return;
        }
        final Bundle ad = this.getCurrentAdBundle();
        if ((ad == null) || ad.isEmpty()) {
            Log.w(TAG, "Ad loading ERROR: NULL ad");
            return;
        }
        context.runOnUiThread(new Runnable() {
            public void run() {
                prepareView();
                displaying = displayContent(ad);
            }
        });

    }

    public void close() {
        clearAd();
        Activity context = contextRef.get();
        if(context == null) return;

        if (displaying) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    View viewContainer = getView();
                    if (viewContainer != null) {
                        getTarget().removeView(viewContainer);
                        displaying = false;
                        viewContainer.destroyDrawingCache();
                    }
                    else {
                        Log.e(TAG, "View layout not found");
                    }
                }
            });
        }
    }

    public void rebuild() {
        close();
        IAdman adman = getAdman();
        if (adman != null && adman.isPlaying()) {
            show();
        }
    }

    @Override
    public void onAdmanEvent(AdmanEvent event) {
        switch (event.getType()) {
            case PREPARE:
            case READY:
                updateStateSuitableVAST();
                break;
            case COMPLETED:
            case SKIPPED:
            case FAILED:
                close();
                break;
        }
    }
    @Override
    public void onPlayerEvent(PlayerEvent event) {
        if (!isSuitableVAST()) return;
        PlayerEvent.Type type = event.getType();
        switch (type) {
            case PLAYING:
                show();
                break;
            case COMPLETE:
            case FAILED:
                close();
                break;
        }
    }

    private Bundle getCurrentAdBundle() {
        IAdman adman = getAdman();
        TritonModule tritonModule = adman != null ? (TritonModule) adman.getModule(TritonModule.ID) : null;
        return tritonModule != null ? tritonModule.getCurrentAdBundle() : null;
    }


    private void updateStateSuitableVAST() {
        Log.d(TAG, "updateStateSuitableVAST");
        Bundle ad = this.getCurrentAdBundle();
        if (ad == null) {
            stateSuitableVAST = StateSuitableVAST.NONE;
            return;
        }
        stateSuitableVAST = StateSuitableVAST.FALSE;
        IAdman adman = getAdman();
        VASTInline vastAd = adman != null ? adman.getCurrentAd(): null;
        if (vastAd != null) {
            String STR_AD_SYSTEM_ADMAN = "instreamatic";
            String lcVastAdSystem = vastAd.adSystem.toLowerCase();
            if (!lcVastAdSystem.contains(STR_AD_SYSTEM_ADMAN)) {
                stateSuitableVAST = StateSuitableVAST.TRUE;
            }
        }
        Log.d(TAG, "PlayerEvent, stateSuitableVAST: " + stateSuitableVAST);
    }

    private boolean isSuitableVAST() {
        if (stateSuitableVAST == StateSuitableVAST.NONE) {
            updateStateSuitableVAST();
        }
        return stateSuitableVAST == StateSuitableVAST.TRUE;
    }

    private IAdmanViewBundle buildViewBundle() {
        Activity context = contextRef.get();
        if (context == null) {
            Log.i(TAG, "Activity is null");
            return null;
        }

        int orientation = context.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            return factory().build(AdmanLayoutType.PORTRAIT, context);
        } else {
            return factory().build(AdmanLayoutType.LANDSCAPE, context);
        }
    }

    private void prepareView() {
        this.bundle = buildViewBundle();
        if (this.bundle == null){
            Log.e(TAG, "Layout has not built");
            return;
        }
        if (bundle.contains(TritonViewTypeExt.BANNER_AD)) {
            bundle.get(TritonViewTypeExt.BANNER_AD).setVisibility(View.INVISIBLE);
        }
        if (bundle.contains(TritonViewTypeExt.VIDEO_AD)) {
            bundle.get(TritonViewTypeExt.VIDEO_AD).setVisibility(View.INVISIBLE);
        }
    }

    private void displayBanner(final Bundle ad, Activity context) {
        mBannerView = (bundle != null && bundle.contains(TritonViewTypeExt.BANNER_AD))
                ? bundle.get(TritonViewTypeExt.BANNER_AD) : null;
        if (mBannerView == null) {
            return;
        }
        Point point = new Point();
        context.getWindowManager().getDefaultDisplay().getSize(point);
        point = mBannerView.getBestBannerSize(ad, point.x, point.y);
        if (point == null) {
            int w = 640;
            int h = 640;
            point = mBannerView.getBestBannerSize(ad, w, h);
        }
        if (point == null) {
            Log.w(TAG, "Banner size not found");
            return;
        }
        mBannerView.setBannerSize(point.x, point.y);
        mBannerView.setVisibility(View.VISIBLE);
    }

    private void displayVideo() {
        mVideoView = (bundle != null && bundle.contains(TritonViewTypeExt.VIDEO_AD))
                ? bundle.get(TritonViewTypeExt.VIDEO_AD) : null;
        if (mVideoView == null) {
            return;
        }
        IAdman adman = this.getAdman();
        final IAudioPlayer player = adman != null ? adman.getPlayer() : null;
        if (player != null) {
            SurfaceHolder holder = mVideoView.getHolder();
            holder.addCallback(holderSurfaceCallback);
            mVideoView.setVisibility(View.VISIBLE);
        }
    }

    protected boolean displayContent(final Bundle ad) {
        boolean isAdd = false;
        ViewGroup target = getTarget();
        if (target != null) {
            View viewContainer = getView();
            if (viewContainer != null) {
                target.addView(viewContainer, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                isAdd = true;
            }
            else{
                Log.e(TAG, "View container not found");
            }
        }
        String mimeType = ad != null ? ad.getString(Ad.MIME_TYPE): null;
        if (mimeType == null) {
            Log.d(TAG, "Warning: No audio/video");
        } else if (mimeType.startsWith("video")) {
            displayVideo();
        } else {
            Activity context = this.contextRef.get();
            if (context != null) {
                displayBanner(ad, context);
                if (mBannerView != null) mBannerView.showAd(ad);
            }
        }
        return isAdd;
    }


    ///////////////////////////////////////////////////////////////////////////
    // Ads playback
    ///////////////////////////////////////////////////////////////////////////
    public void clearAd() {
        // Clear the previous banner content without destroying the view.
        if (mBannerView != null) {
            mBannerView.clearBanner();
            mBannerView = null;
        }
        // Cancel the video playback
        if (mVideoView != null) {
            mVideoView.setOnClickListener(null);
            mVideoView.setVisibility(View.GONE);
            mVideoView = null;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "Playback completed.");
        close();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "Playback error: " + what + '/' + extra);
        close();
        return true;
    }

    private class SurfaceHolderCallback implements SurfaceHolder.Callback {
        public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k) {
            Log.d(TAG, "surfaceChanged called");

        }

        public void surfaceDestroyed(SurfaceHolder surfaceholder) {
            Log.d(TAG, "surfaceDestroyed called");
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d(TAG, "surfaceCreated called");
            IAdman adman = getAdman();
            final IAudioPlayer player = adman != null ? adman.getPlayer() : null;
            if (mVideoView != null && player != null) {
                mVideoView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "Video clicked.");
                        final Bundle ad = getCurrentAdBundle();
                        if ((ad == null) || ad.isEmpty()) {
                            Log.w(TAG, "Ad loading ERROR: NULL ad");
                            return;
                        }

                        //track video clicks
                        Ad.trackVideoClick(ad);

                        final String clickUrl = ad.getString(Ad.VIDEO_CLICK_THROUGH_URL);
                        Log.d(TAG, "Video clicked url: " + clickUrl);
                        if ((clickUrl == null) || clickUrl.isEmpty()) {
                            return;
                        }

                        Activity context = contextRef.get();
                        if (context == null) {
                            Log.i(TAG, "Activity is null");
                            return;
                        }

                        ActionUtil.openUrl(context, clickUrl);
                    }
                });
                MediaPlayer mMediaPlayer = player.getMediaPlayer();
                mMediaPlayer.setDisplay(mVideoView.getHolder());
            }
        }

    }

}
