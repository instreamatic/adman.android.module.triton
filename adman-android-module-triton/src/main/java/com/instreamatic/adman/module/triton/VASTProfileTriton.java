package com.instreamatic.adman.module.triton;

import android.os.Bundle;
import android.util.Log;

import com.tritondigital.ads.AdParser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class VASTProfileTriton {
    final private static String TAG = "VASTProfileTriton";

    private Bundle currentAd;
    private byte[] bytes;

    public Bundle getAd(){
        return this.currentAd;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public void updateBytes(byte[] bytes) {
        this.currentAd = this.getAdFromBytes(bytes);
        if ((this.currentAd != null) || !this.currentAd.isEmpty()) {
            this.bytes = bytes;
        }
    }

    // utils methods
    public void clear() {
        this.currentAd = null;
        this.bytes = null;
    }

    private Bundle getAdFromBytes(byte[] byteArray) {
        AdParser adParser = new AdParser();
        Bundle ad = null;
        try {
            InputStream is = new ByteArrayInputStream(byteArray);
            ad = adParser.a(is);
            is.close();
        } catch (Exception e) {
            Log.e(TAG, "VAST a_parser exception", e);
        }
        return ad;
    }

}
