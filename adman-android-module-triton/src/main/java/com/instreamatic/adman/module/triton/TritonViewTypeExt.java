package com.instreamatic.adman.module.triton;

import android.view.SurfaceView;
import android.view.View;

import com.instreamatic.adman.view.AdmanViewType;
import com.tritondigital.ads.SyncBannerView;

final public class TritonViewTypeExt<T extends View> extends AdmanViewType<T> {
    public static final TritonViewTypeExt<SyncBannerView> BANNER_AD = new TritonViewTypeExt(SyncBannerView.class);
    public static final TritonViewTypeExt<SurfaceView> VIDEO_AD = new TritonViewTypeExt(SurfaceView.class);

    private TritonViewTypeExt(Class<T> type) {
        super(type);
    }
}
