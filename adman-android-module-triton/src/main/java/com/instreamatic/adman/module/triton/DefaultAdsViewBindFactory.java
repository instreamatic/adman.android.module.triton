package com.instreamatic.adman.module.triton;

import android.app.Activity;

import com.instreamatic.adman.view.AdmanViewType;
import com.instreamatic.adman.view.IAdmanViewBundle;
import com.instreamatic.adman.view.core.AdmanViewBindFactory;
import com.instreamatic.adman.view.core.AdmanViewBundle;

import java.util.HashMap;
import java.util.Map;

public class DefaultAdsViewBindFactory  extends AdmanViewBindFactory {
    final Map<AdmanViewType, Integer> bindings = new HashMap<AdmanViewType, Integer>() {{
        put(TritonViewTypeExt.BANNER_AD, R.id.banner);
        put(TritonViewTypeExt.VIDEO_AD, R.id.surfaceView);
    }};

    @Override
    protected IAdmanViewBundle buildPortrait(Activity activity) {
        return AdmanViewBundle.fromLayout(activity, R.layout.ads_custom, bindings);
    }

    @Override
    protected IAdmanViewBundle buildLandscape(Activity activity) {
        return AdmanViewBundle.fromLayout(activity, R.layout.ads_custom, bindings);
    }

    @Override
    protected IAdmanViewBundle buildVoice(Activity activity) {
        return AdmanViewBundle.fromLayout(activity, R.layout.ads_custom, bindings);
    }
}
