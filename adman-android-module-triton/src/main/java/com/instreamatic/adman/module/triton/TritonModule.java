package com.instreamatic.adman.module.triton;

import android.os.Bundle;

import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.module.BaseAdmanModule;

public class TritonModule extends BaseAdmanModule implements RequestEvent.Listener{
    final private static String TAG = "TritonModule";
    final public static String ID = TritonModule.class.getSimpleName();

    private VASTProfileTriton profileTriton = new VASTProfileTriton();

    @Override
    public String getId(){
        return ID;
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[] {RequestEvent.TYPE};
    }

    //RequestEvent.Listener
    @Override
    public void onRequestEvent(RequestEvent event) {
        switch (event.getType()) {
            case LOAD:
                profileTriton.clear();
                break;
            case NONE:
                break;
            case SUCCESS:
                profileTriton.clear();
                profileTriton.updateBytes(event.byteArray);
                break;
        }
    }

    public Bundle getCurrentAdBundle() {
        return this.profileTriton != null ? this.profileTriton.getAd() : null;
    }
}
