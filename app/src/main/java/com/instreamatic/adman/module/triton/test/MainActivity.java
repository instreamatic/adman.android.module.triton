package com.instreamatic.adman.module.triton.test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.util.Log;

import com.google.android.material.snackbar.Snackbar;
import com.instreamatic.adman.Adman;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.module.triton.DefaultAdsView;
import com.instreamatic.adman.module.triton.TritonModule;
import com.instreamatic.adman.view.generic.DefaultAdmanView;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.vast.model.VASTInline;

public class MainActivity extends AppCompatActivity implements AdmanEvent.Listener, RequestEvent.Listener{
    final private static String TAG = "MainActivity";
    private IAdman adman;
    private DefaultAdsView tritonAdsView;
    private DefaultAdmanView instreamaticAdsView;
    private View startView;
    private View loadingView;

    private enum AdViewKind{
        NONE,
        ТRITON,
        INSTREAMATIC
    }
    private AdViewKind adViewKind = AdViewKind.NONE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //AndroidManifest.xml   android:configChanges="keyboardHidden|orientation|screenSize"

        checkPermission();
        initAdman();
        startView = findViewById(R.id.start);
        loadingView = findViewById(R.id.loading);
        startView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adman.start();
                    }
                });
            }
        });
    }

    @Override
    protected void onPause() {
        clearAdsView();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initAdsView();
        if (adman!= null && adman.isPlaying()){
            showAdsView();
        }
    }

    private void initAdman() {
        AdmanRequest.Builder builder = getAdRequestAdman();
        adman = new Adman(this, builder.build());

        adman.bindModule(new AdmanVoice(this));
        adman.getDispatcher().addListener(AdmanEvent.TYPE, this);
        adman.getDispatcher().addListener(RequestEvent.TYPE, this);

        adman.bindModule(new TritonModule());
    }

    private AdmanRequest.Builder getAdRequestAdman() {
        AdmanRequest.Builder request_builder = null;
        //banner
        //request_builder = new AdmanRequest.Builder().setAdUrlAPI("https://od-spy.live.streamtheworld.com/ondemand/ars?stid=170713&type=preroll&scenario=vast-wrapper");

        //video
        request_builder = new AdmanRequest.Builder().setAdUrlAPI("https://cmod719.live.streamtheworld.com/ondemand/ars?type=preroll&stid=284343&version=1.6.9&dist=vvv");

        //voice
        /*
        request_builder = new AdmanRequest.Builder()
                .setSiteId(1249)
                .setRegion(Region.GLOBAL)
                .setType(Type.VOICE);
        /**/
        return  request_builder;
    }

    private AdViewKind getAdViewKind()  {
        VASTInline vastAd = adman != null ? adman.getCurrentAd(): null;
        if (vastAd != null) {
            String STR_AD_SYSTEM_ADMAN = "instreamatic";
            String lcVastAdSystem = vastAd.adSystem.toLowerCase();
            return lcVastAdSystem.contains(STR_AD_SYSTEM_ADMAN) ? AdViewKind.INSTREAMATIC : AdViewKind.ТRITON;
        }
        return AdViewKind.NONE;
    }

    private void clearAdsView() {
        if (tritonAdsView != null) {
            tritonAdsView.close();
            adman.unbindModule(tritonAdsView);
            tritonAdsView = null;
        }
        if (instreamaticAdsView != null) {
            instreamaticAdsView.close();
            adman.unbindModule(instreamaticAdsView);
            instreamaticAdsView = null;
        }
    }

    private void initAdsView() {
        AdViewKind viewKind = getAdViewKind();
        if (viewKind == AdViewKind.ТRITON) {
            tritonAdsView = new DefaultAdsView(this);
            adman.bindModule(tritonAdsView);
        }
        else {
            instreamaticAdsView = new DefaultAdmanView(this);
            adman.bindModule(instreamaticAdsView);
        }
    }

    private void showAdsView() {
        if (tritonAdsView != null) {
            tritonAdsView.show();
        }
        else if (instreamaticAdsView != null) {
            instreamaticAdsView.show();
        }
    }

    @Override
    public void onRequestEvent(RequestEvent event) {
        Log.d(TAG, "onAdmanEvent: " + event.getType().name());
        switch (event.getType()) {
            case LOAD:
            case FAILED:
            case NONE:
                clearAdsView();
                break;
            case SUCCESS:
                initAdsView();
                break;
        }
    }

    @Override
    public void onAdmanEvent(AdmanEvent event) {
        Log.d(TAG, "onAdmanEvent: " + event.getType().name());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (event.getType()) {
                    case PREPARE:
                        startView.setVisibility(View.GONE);
                        loadingView.setVisibility(View.VISIBLE);
                        break;
                    case NONE:
                    case FAILED:
                    case SKIPPED:
                    case COMPLETED:
                        startView.setVisibility(View.VISIBLE);
                        loadingView.setVisibility(View.GONE);
                        break;
                    case STARTED:
                        loadingView.setVisibility(View.GONE);
                        break;
                }
            }
        });
    }
    /**
     * Mic access
     **/
    final private static int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private void checkPermission() {
        View mLayout = (View) findViewById(R.id.main_layout);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                try {
                    Snackbar.make(mLayout, R.string.permission_record_audio_rationale,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ActivityCompat.requestPermissions(MainActivity.this,
                                            new String[]{Manifest.permission.RECORD_AUDIO},
                                            PERMISSIONS_REQUEST_RECORD_AUDIO);
                                }
                            })
                            .show();
                } catch (Exception ex) {

                }
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        PERMISSIONS_REQUEST_RECORD_AUDIO);

                // PERMISSIONS_REQUEST_RECORD_AUDIO is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}